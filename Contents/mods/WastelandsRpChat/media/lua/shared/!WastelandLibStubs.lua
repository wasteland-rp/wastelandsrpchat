if getActivatedMods():contains("WastelandLib") then return end

WL_Utils = {}
WL_Utils.MagicSpace = "� �� "

--- Returns true if the player has any staff access level (Admin, Moderator, Overseer, GM or Observer)
--- @param player IsoPlayer
--- @return boolean
function WL_Utils.isStaff(player)
    if not isClient() and not isServer() then return true end -- SP
    if not player then return false end
    local accessLevel = player:getAccessLevel()
    return accessLevel ~= "None"
end

--- Returns true if the player is a moderator or admin
--- @param player IsoPlayer|nil will use getPlayer() if nil
--- @return boolean
function WL_Utils.canModerate(player)
    if not isClient() and not isServer() then return true end -- SP
    if not player then player = getPlayer() end
    local accessLevel = player:getAccessLevel()
    return accessLevel == "Moderator" or accessLevel == "Admin"
end

--- Add a WL_FakeMessage to the chat window
--- @param message WL_FakeMessage
function WL_Utils.addFakeMessageToChatWindow(message)
    if not ISChat or not ISChat.instance or not ISChat.instance.chatText then return end
    local line = message:getTextWithPrefix()
    local chatText = ISChat.instance.chatText
    if message:getChatID() then
        for _,v in ipairs(ISChat.instance.tabs) do
            if v.tabID == message:getChatID() then
                chatText = v
                break
            end
        end
    end
    if chatText.tabTitle ~= ISChat.instance.chatText.tabTitle then
        local alreadyExist = false;
        for i,blinkedTab in ipairs(ISChat.instance.panel.blinkTabs) do
            if blinkedTab == chatText.tabTitle then
                alreadyExist = true;
                break;
            end
        end
        if alreadyExist == false then
            table.insert(ISChat.instance.panel.blinkTabs, chatText.tabTitle);
        end
    end
    local vscroll = chatText.vscroll
    local scrolledToBottom = (chatText:getScrollHeight() <= chatText:getHeight()) or (vscroll and vscroll.pos == 1)
    if #chatText.chatTextLines > ISChat.maxLine then
        local newLines = {}
        for i,v in ipairs(chatText.chatTextLines) do
            if i ~= 1 then
                table.insert(newLines, v)
            end
        end
        table.insert(newLines, line .. " <LINE> ")
        chatText.chatTextLines = newLines
    else
        table.insert(chatText.chatTextLines, line .. " <LINE> ")
    end
    chatText.text = ""
    local newText = ""
    for i,v in ipairs(chatText.chatTextLines) do
        if i == #chatText.chatTextLines then
            v = string.gsub(v, " <LINE> $", "")
        end
        newText = newText .. v
    end
    chatText.text = newText
    table.insert(chatText.chatMessages, message)
    chatText:paginate()
    if scrolledToBottom then
        chatText:setYScroll(-100000)
    end
end

--- Add a message to the chat window
--- @param text string
--- @param options WL_ChatOptions|nil
function WL_Utils.addToChat(text, options)
    local message = WL_FakeMessage:new(text, options)
    WL_Utils.addFakeMessageToChatWindow(message)
end

--- Add a message to the chat window with a red color
--- @param text string
--- @param options WL_ChatOptions|nil
function WL_Utils.addErrorToChat(text, options)
    options = options or {}
    options.color = "1.0,0.2,0.2"
    WL_Utils.addToChat(text, options)
end

--- Add a message to the chat window with a blue color
--- @param text string
--- @param options WL_ChatOptions|nil
function WL_Utils.addInfoToChat(text, options)
    options = options or {}
    options.color = "0.2,0.2,1.0"
    WL_Utils.addToChat(text, options)
end



--- @class WL_ChatOptions
--- @field author string|nil
--- @field radioChannel number|nil
--- @field datetimeStr string|nil
--- @field color string|nil
--- @field showOverhead boolean|nil
--- @field chatId number|nil


--- @class WL_FakeMessage
--- @field text string
--- @field author string
--- @field radioChannel number
--- @field datetimeStr string
--- @field color string
--- @field showOverhead boolean
--- @field chatId number
WL_FakeMessage = {}

---Create a new WL_FakeMessage
---@param text string
---@param options WL_ChatOptions|nil
---@return WL_FakeMessage
function WL_FakeMessage:new(text, options)
    options = options or {}

    local o = {}
    setmetatable(o, self)
    o.__index = self

    o.text = text
    o.author = options.author or ""
    o.radioChannel = options.radioChannel or 0
    o.datetimeStr = options.datetimeStr or nil
    o.color = options.color or nil
    o.showOverhead = options.showOverhead or false
    o.chatId = options.chatId or 1
    return o
end

function WL_FakeMessage:setText(text)
    self.text = text
end
function WL_FakeMessage:getText()
    if self.color then
        return "<RGB:" .. self.color .. ">" .. self.text
    end
    return self.text
end
function WL_FakeMessage:getAuthor()
    return self.author
end
function WL_FakeMessage:getRadioChannel()
    return self.radioChannel
end
function WL_FakeMessage:isServerAlert()
    return false
end
function WL_FakeMessage:getTextWithPrefix()
    local message = self:getText()
    if ISChat.instance.showTimestamp and self.datetimeStr then
        message = "<RGB:0.4,0.4,0.4>[" .. self.datetimeStr .. "] " .. message
    end
    if ISChat.instance.chatFont then
        message = "<SIZE:" .. ISChat.instance.chatFont .. ">" .. message
    end
    return message
end
function WL_FakeMessage:isOverHeadSpeech()
    return self.showOverhead
end
function WL_FakeMessage:getChatID()
    return self.chatId
end
function WL_FakeMessage:getDatetimeStr()
    return self.datetimeStr
end

function WL_FakeMessage:setOverHeadSpeech() end
function WL_FakeMessage:setShouldAttractZombies() end

--- @class Bounds
--- @field x1 number
--- @field y1 number
--- @field x2 number
--- @field y2 number
---
--- @class Color
--- @field r number
--- @field g number
--- @field b number
--- @field a number
---
--- @class Center
--- @field x number
--- @field y number
--- @field z number
---
--- @class GroundHightlighter
--- @field type string none | square | circle
--- @field bounds Bounds
--- @field radius number
--- @field center Center
--- @field color Color
--- @field xray boolean
GroundHightlighter = {}

--- @return GroundHightlighter
function GroundHightlighter:new()
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.type = "none"
    o.bounds = {x1 = 0, y1 = 0, x2 = 0, y2 = 0}
    o.radius = 0
    o.center = {x = 0, y = 0, z = 0}
    o.color = {r = 0.0, g = 1.0, b = 0, a = 1.0}
    o.xray = false
    return o
end

function GroundHightlighter:isPointInRadius(x, y)
    local dx = x - self.center.x
    local dy = y - self.center.y
    return (dx * dx) + (dy * dy) <= (self.radius * self.radius)
end

function GroundHightlighter:isVisible(x, y)
    if self.type == "square" then
        return x >= self.bounds.x1 and x <= self.bounds.x2 and y >= self.bounds.y1 and y <= self.bounds.y2
    end
    if self.type == "circle_edge" then
        -- should only highlight the edge of the circle
        local dx = x - self.center.x
        local dy = y - self.center.y
        local dist = (dx * dx) + (dy * dy)
        local r2 = self.radius * self.radius
        return dist >= r2 - 1.5 and dist <= r2 + 1.5
    end
    return self:isPointInRadius(x, y)
end

function GroundHightlighter:tryHighlightWorldSquare(sq, enabled)
    if enabled and not self:isVisible(sq:getX(), sq:getY()) then
        return
    end
    local objs = sq:getObjects()
    for i = 0, objs:size() - 1 do
        local obj = objs:get(i)
        if obj:isFloor() or self.xray or not enabled then
            obj:setHighlighted(enabled, false)
            if enabled then
                obj:setHighlightColor(self.color.r, self.color.g, self.color.b, self.color.a)
            end
        end
    end
end

function GroundHightlighter:setHightlighted(enabled)
    local cell = getCell()
    for x = self.bounds.x1, self.bounds.x2 do
        for y = self.bounds.y1, self.bounds.y2 do
            local sq = cell:getOrCreateGridSquare(x, y, self.center.z)
            if sq then
                self:tryHighlightWorldSquare(sq, enabled)
            end
        end
    end
end

function GroundHightlighter:remove()
    if self.type ~= "none" then
        self:setHightlighted(false)
        self.type = "none"
    end
end

function GroundHightlighter:setColor(r, g, b, a)
    self.color.r = r
    self.color.g = g
    self.color.b = b
    self.color.a = a or 1.0
    if self.type ~= "none" then
        self:setHightlighted(false)
        self:setHightlighted(true)
    end
end

function GroundHightlighter:enableXray(enabled)
    self.xray = enabled
    if self.type ~= "none" then
        self:remove()
        self:setHightlighted(true)
    end
end

function GroundHightlighter:highlightSquare(x1, y1, x2, y2, z)
    self:remove()
    self.type = "square"
    self.bounds.x1 = math.floor(x1)
    self.bounds.y1 = math.floor(y1)
    self.bounds.x2 = math.floor(x2)
    self.bounds.y2 = math.floor(y2)
    self.center.x = math.floor((x1 + x2) / 2)
    self.center.y = math.floor((y1 + y2) / 2)
    self.center.z = z or 0
    self:setHightlighted(true)
end

function GroundHightlighter:highlightCircle(x, y, radius, z)
    self:remove()
    self.type = "circle"
    self.radius = radius
    self.center.x = math.floor(x)
    self.center.y = math.floor(y)
    self.center.z = z or 0
    self.bounds.x1 = math.floor(x - radius)
    self.bounds.y1 = math.floor(y - radius)
    self.bounds.x2 = math.floor(x + radius)
    self.bounds.y2 = math.floor(y + radius)
    self:setHightlighted(true)
end
