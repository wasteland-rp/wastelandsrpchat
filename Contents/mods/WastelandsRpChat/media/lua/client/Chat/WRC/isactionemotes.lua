local radiosShutOff = {}
local radiosShutOffPlayer = nil

local function restoreRadios()
    for i=1,#radiosShutOff do
        WRU_Utils.setRadioPowerInstant(radiosShutOffPlayer, radiosShutOff[i], true)
    end
    radiosShutOff = {}
end

local function muteRadios(player)
    radiosShutOffPlayer = player
    radiosShutOff = WRU_Utils.getPlayerRadios(player, true, true)
    for i=1,#radiosShutOff do
        WRU_Utils.setRadioPowerInstant(player, radiosShutOff[i], false)
    end
end

local function sendEmote(emote)
    local player = getPlayer()
    if player:isGhostMode() then return end

    emote = WRC.Parsing.PrependPlayerData(player, "[emote]/me " .. emote)
    muteRadios(player)
    processSayMessage(emote)
    restoreRadios()
end

local walkiePhrases = {
    "interacted with their walkie.",
    "adjusted a walkie's settings.",
    "tweaked their walkie.",
    "checked a walkie.",
    "handled a walkie.",
    "poked at their walkie."
}

local original_ISRadioAction_perform = ISRadioAction.perform
function ISRadioAction:perform()
    original_ISRadioAction_perform(self)
    if self.device
    and instanceof(self.device, "InventoryItem")
    and self.character:getInventory():containsRecursive(self.device)
    and self.device:getType():sub(1, 12) == "WalkieTalkie"  then
        local num = ZombRand(#walkiePhrases - 1) + 1
        sendEmote(walkiePhrases[num])
    end
end