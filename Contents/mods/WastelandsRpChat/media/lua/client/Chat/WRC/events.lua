if not isClient() then return end -- only in MP
WRC = WRC or {}
WRC.Events = WRC.Events or {}
WRC.Events.IsFirstSync = true

function WRC.Events.OnReceiveGlobalModData(key, modData)
    if key == "WRC_PlayerColors" then
        WRC.PlayerColors = modData
    elseif key == "WRC_PlayerLanguages" then
        WRC.PlayerLanguages = modData
    elseif key == "WRC_PlayerModifiers" then
        WRC.PlayerModifiers = modData
        WRC.Afk.CheckLocalPlayersForAfk()
        if WRC.Events.IsFirstSync then
            WRC.Events.IsFirstSync = false
            if WRC.Afk.IsSelfAfk() then
                WRC.Afk.StartAfk()
            end
        end
    elseif key == "WRC_PlayerNames" then
        WRC.PlayerNames = modData
        if not WRC.PlayerNames[getPlayer():getUsername()] then
            WRC.Meta.SetName(getPlayer():getDescriptor():getForename())
        end
    elseif key == "WRC_PlayerStatus" then
        WRC.PlayerStatus = modData
    end
end

function WRC.Events.OnConnected()
	ModData.request("WRC_PlayerColors")
	ModData.request("WRC_PlayerLanguages")
    ModData.request("WRC_PlayerModifiers")
    ModData.request("WRC_PlayerNames")
    ModData.request("WRC_PlayerStatus")
end

function WRC.Events.onServerCommand(module, command, args)
    if module ~= "WRC" then return end

    if command == "onTyping" then
        WRC.Indicator.players[args[1]] = getTimestampMs()
    elseif command == "onCleared" then
        WRC.Indicator.players[args[1]] = nil
    elseif command == "SetPlayerColor" then
        local player = args[1]
        local r = args[2]
        local g = args[3]
        local b = args[4]
        WRC.PlayerColors[player] = {r = r, g = g, b = b}
    elseif command == "SetPlayerLanguage" then
        local player = args[1]
        local language = args[2]
        WRC.PlayerLanguages[player] = language
    elseif command == "SetModifier" then
        local player = args[1]
        local direction = args[2]
        local modifier = args[3]
        WRC.PlayerModifiers[player] = WRC.PlayerModifiers[player] or {}
        if direction == "enable" then
            WRC.PlayerModifiers[player][modifier] = true
        elseif direction == "disable" then
            WRC.PlayerModifiers[player][modifier] = nil
        end
    elseif command == "SetPlayerName" then
        local player = args[1]
        local name = args[2]
        WRC.PlayerNames[player] = name
    elseif command == "SetPlayerStatus" then
        local player = args[1]
        local status = args[2]
        WRC.PlayerStatus[player] = status
    elseif command == "AddKnownLanguage" then
        local languageData = WRC.Languages[args[1]]
        if languageData then
            WRC.Meta.AddKnownLanguage(args[1])
            WL_Utils.addInfoToChat("You have learned " .. languageData.name)
        end
    elseif command == "RemoveKnownLanguage" then
        local languageData = WRC.Languages[args[1]]
        if languageData then
            WRC.Meta.RemoveKnownLanguage(args[1])
            WL_Utils.addInfoToChat("You have forgotten " .. languageData.name)
        end
    elseif command == "InvitePrivate" then
        local otherPlayer = args[1]
        if WRC.Meta.HasPrivate(true) then
            sendClientCommand(getPlayer(), "WRC", "PrivateUnavailable", {otherPlayer})
        else
            WRC.Meta.OnPrivateInvite(otherPlayer)
        end
    elseif command == "PrivateUnavailable" then
        local otherPlayer = args[1]
        WL_Utils.addErrorToChat(otherPlayer .. " is unable to private chat.")
    elseif command == "AcceptPrivateInvite" then
        local otherPlayer = args[1]
        WRC.Meta.StartPrivate(otherPlayer)
        ISChat.instance.panel:activateView("Private")
        WL_Utils.addInfoToChat("Private chat started with " .. WRC.Meta.GetName(otherPlayer) .. ".")
    elseif command == "DeclinePrivateInvite" then
        local otherPlayer = args[1]
        WL_Utils.addInfoToChat(otherPlayer .. " declined your private chat invite.")
    elseif command == "PrivateChat" then
        local otherPlayerUsername = args[1]
        local message = args[2]
        WRC.Handlers.AddPrivateMessage(otherPlayerUsername, message)
    elseif command == "StopPrivate" then
        ISChat.instance.panel:activateView("Private")
        local name = WRC.Meta.PrivatePartner and WRC.Meta.GetName(WRC.Meta.PrivatePartner) or "Unknown"
        WL_Utils.addInfoToChat("Private with " .. name .. " ended.")
        WRC.Meta.StopPrivate(true)
    elseif command == "StaffChat" then
        local sourceUsername = args[1]
        local message = args[2]
        WRC.Handlers.AddStaffMessage(sourceUsername, message)
    end

end

Events.OnReceiveGlobalModData.Add(WRC.Events.OnReceiveGlobalModData)
Events.OnConnected.Add(WRC.Events.OnConnected)
Events.OnServerCommand.Add(WRC.Events.onServerCommand)
Events.OnTick.Add(WRC.Indicator.update)
Events.EveryOneMinute.Add(WRC.Afk.CheckLocalPlayersForAfk)
