if not isClient() then return end -- only in MP
WRC = WRC or {}

-- Define possible languages
WRC.Languages = {}
WRC.Languages["en"] = {
    name = "English",
    canPartiallyUnderstand = {},
}
WRC.Languages["asl"] = {
    name = "American Sign Language",
    canPartiallyUnderstand = {},
}
WRC.Languages["es"] = {
    name = "Spanish",
    canPartiallyUnderstand = {"pt"},
}
WRC.Languages["fr"] = {
    name = "French",
    canPartiallyUnderstand = {},
}
WRC.Languages["de"] = {
    name = "German",
    canPartiallyUnderstand = {"nl"},
}
WRC.Languages["it"] = {
    name = "Italian",
    canPartiallyUnderstand = {"es", "pt", "ro"},
}
WRC.Languages["ru"] = {
    name = "Russian",
    canPartiallyUnderstand = {"uk", "bg"},
}
WRC.Languages["zh"] = {
    name = "Chinese",
    canPartiallyUnderstand = {},
}
WRC.Languages["ja"] = {
    name = "Japanese",
    canPartiallyUnderstand = {},
}
WRC.Languages["ko"] = {
    name = "Korean",
    canPartiallyUnderstand = {},
}
WRC.Languages["pt"] = {
    name = "Portuguese",
    canPartiallyUnderstand = {"es"},
}
WRC.Languages["pl"] = {
    name = "Polish",
    canPartiallyUnderstand = {},
}
WRC.Languages["sv"] = {
    name = "Swedish",
    canPartiallyUnderstand = {"no", "da"},
}
WRC.Languages["nl"] = {
    name = "Dutch",
    canPartiallyUnderstand = {"de", "af"},
}
WRC.Languages["cs"] = {
    name = "Czech",
    canPartiallyUnderstand = {"sk"},
}
WRC.Languages["hu"] = {
    name = "Hungarian",
    canPartiallyUnderstand = {},
}
WRC.Languages["fi"] = {
    name = "Finnish",
    canPartiallyUnderstand = {},
}
WRC.Languages["tr"] = {
    name = "Turkish",
    canPartiallyUnderstand = {},
}
WRC.Languages["no"] = {
    name = "Norwegian",
    canPartiallyUnderstand = {"sv", "da"},
}
WRC.Languages["da"] = {
    name = "Danish",
    canPartiallyUnderstand = {"sv", "no"},
}
WRC.Languages["ro"] = {
    name = "Romanian",
    canPartiallyUnderstand = {"it"},
}
WRC.Languages["bg"] = {
    name = "Bulgarian",
    canPartiallyUnderstand = {"ru", "sr", "mk"},
}
WRC.Languages["el"] = {
    name = "Greek",
    canPartiallyUnderstand = {},
}
WRC.Languages["uk"] = {
    name = "Ukrainian",
    canPartiallyUnderstand = {"ru"},
}
WRC.Languages["sk"] = {
    name = "Slovak",
    canPartiallyUnderstand = {"cs"},
}
WRC.Languages["hr"] = {
    name = "Croatian",
    canPartiallyUnderstand = {"sr"},
}
WRC.Languages["sr"] = {
    name = "Serbian",
    canPartiallyUnderstand = {"hr", "bg", "mk"},
}
WRC.Languages["sl"] = {
    name = "Slovenian",
    canPartiallyUnderstand = {},
}
WRC.Languages["lt"] = {
    name = "Lithuanian",
    canPartiallyUnderstand = {},
}
WRC.Languages["lv"] = {
    name = "Latvian",
    canPartiallyUnderstand = {},
}
WRC.Languages["et"] = {
    name = "Estonian",
    canPartiallyUnderstand = {},
}
WRC.Languages["ar"] = {
    name = "Arabic",
    canPartiallyUnderstand = {},
}
WRC.Languages["he"] = {
    name = "Hebrew",
    canPartiallyUnderstand = {},
}
WRC.Languages["th"] = {
    name = "Thai",
    canPartiallyUnderstand = {},
}
WRC.Languages["vi"] = {
    name = "Vietnamese",
    canPartiallyUnderstand = {},
}
WRC.Languages["id"] = {
    name = "Indonesian",
    canPartiallyUnderstand = {"ms"},
}
WRC.Languages["ms"] = {
    name = "Malay",
    canPartiallyUnderstand = {"id"},
}
WRC.Languages["hi"] = {
    name = "Hindi",
    canPartiallyUnderstand = {"ur"},
}
WRC.Languages["bn"] = {
    name = "Bengali",
    canPartiallyUnderstand = {},
}
WRC.Languages["fa"] = {
    name = "Persian",
    canPartiallyUnderstand = {},
}
WRC.Languages["ur"] = {
    name = "Urdu",
    canPartiallyUnderstand = {"hi"},
}
WRC.Languages["sw"] = {
    name = "Swahili",
    canPartiallyUnderstand = {},
}
WRC.Languages["af"] = {
    name = "Afrikaans",
    canPartiallyUnderstand = {"nl"},
}
WRC.Languages["eo"] = {
    name = "Esperanto",
    canPartiallyUnderstand = {},
}
WRC.Languages["is"] = {
    name = "Icelandic",
    canPartiallyUnderstand = {},
}
WRC.Languages["cy"] = {
    name = "Welsh",
    canPartiallyUnderstand = {},
}
WRC.Languages["yi"] = {
    name = "Yiddish",
    canPartiallyUnderstand = {},
}
WRC.Languages["la"] = {
    name = "Latin",
    canPartiallyUnderstand = {},
}
WRC.Languages["ga"] = {
    name = "Gaelic",
    canPartiallyUnderstand = {},
}
WRC.Languages["hw"] = {
    name = "Hawaiian",
    canPartiallyUnderstand = {},
}