
local stories = {}

table.insert(stories, {
    title = "The Final Stand",
    messages = {
        "[UN:Johnny Boy]/me slams his hand on table, saying \"Oh no.. the walls...\"",
        "[UN:Laura Sue]/me says with panick in her voice \"What?! What's going on?\"",
        "[UN:Johnny Boy]/me takes a deep breath, and sorrowfully says \"They broke through.\"",
        "[UN:_]/env The sound of metal scraping is heard in the background.",
        "[UN:Laura Sue]/me pauses before saying \"I love you Johnny. I always will.\"",
        "[UN:_]/env Windows shatter, and groans get louder and loud. The two yell a war cry.",
    }
})

table.insert(stories, {
    title = "Last Broadcast",
    messages = {
        "[UN:_]/env Static noise, followed by a faint tune playing in the background.",
        "[UN:DJ Rick]/me says with a forced cheerfulness \"This might be our last night with the airwaves, folks.\"",
        "[UN:Caller Jane]/me 's voice cracks over the line \"Is there any hope left, Rick?\"",
        "[UN:DJ Rick]/me sighs, the weight of reality sinking in \"Hope's what you make it, Jane. Keep fighting.\"",
        "[UN:_]/env The sound of the radio station's door being pounded on echoes.",
        "[UN:DJ Rick]/me whispers, fear palpable in his voice \"They're here. Remember to keep the music alive.\"",
        "[UN:_]/env The transmission cuts to static, with a haunting melody playing till it fades out."
    }
})

table.insert(stories, {
    title = "The Safehouse",
    messages = {
        "[UN:Max]/me breathes heavily, whispering \"This is it, the place looks secure.\"",
        "[UN:Sara]/me 's voice trembles \"But for how long? We can't keep running forever.\"",
        "[UN:Max]/me tries to sound optimistic \"We'll fortify it. Make it a home.\"",
        "[UN:_]/env Sounds of barricading doors and windows are heard.",
        "[UN:Sara]/me softly says \"I found some seeds, Max. We could start a garden.\"",
        "[UN:_]/env A moment of silence, then the distant moan of zombies grows slightly louder.",
        "[UN:Max]/me firmly says \"Let's focus on surviving tonight. We'll think about tomorrow, tomorrow.\""
    }
})

table.insert(stories, {
    title = "Echoes of the Past",
    messages = {
        "[UN:_]/env The soft hum of an old cassette player starting up.",
        "[UN:Old Man Jenkins]/me 's voice, crackled with age, begins \"I remember the days before the madness...\"",
        "[UN:_]/env The sound of children laughing and playing in a park.",
        "[UN:Old Man Jenkins]/me continues with a sigh \"We took it all for granted, our daily lives, our families...\"",
        "[UN:_]/env A sudden silence, then the eerie sound of wind blowing through empty streets.",
        "[UN:Old Man Jenkins]/me 's voice, now somber \"Now, all that's left are memories. We survive, yes, but at what cost?\"",
        "[UN:_]/env The cassette player clicks and whirrs, the tape reaching its end, leaving a haunting silence."
    }
})

table.insert(stories, {
    title = "Birthday Surprise",
    messages = {
        "[UN:_]/env The muffled sound of people hiding.",
        "[UN:Mom]/me whispers excitedly \"Is everyone ready? He's coming!\"",
        "[UN:Little Timmy]/me tries to whisper but fails \"I can't wait to see daddy's face!\"",
        "[UN:_]/env The sound of a key turning in the door.",
        "[UN:Dad]/me sounds surprised as everyone yells \"Surprise! Happy Birthday!\"",
        "[UN:_]/env Laughter and the sound of a small celebration, with clinking glasses.",
        "[UN:Dad]/me , overwhelmed with joy, says \"This is the best birthday ever. Thank you, everyone.\""
    }
})

table.insert(stories, {
    title = "First Guitar Lesson",
    messages = {
        "[UN:Grandpa Joe]/me 's voice is encouraging \"Hold it like this, gently. Ready for your first chord?\"",
        "[UN:Jenny]/me sounds frustrated but determined \"It sounds awful. I'll never get this.\"",
        "[UN:_]/env The awkward strumming of guitar strings, slowly improving.",
        "[UN:Grandpa Joe]/me laughs softly \"Everyone starts somewhere, Jenny. You're doing great.\"",
        "[UN:_]/env A moment of quiet, then a simple melody played with a few mistakes.",
        "[UN:Jenny]/me excitedly says \"I did it, Grandpa! Did you hear that?\"",
        "[UN:Grandpa Joe]/me , proudly \"Loud and clear, kiddo. You're a natural.\""
    }
})

table.insert(stories, {
    title = "Rainy Day Reflections",
    messages = {
        "[UN:_]/env The gentle patter of rain against the window.",
        "[UN:Sarah]/me 's voice is soft and contemplative \"Rain always makes me reflective, thinking about the paths not taken...\"",
        "[UN:_]/env The sound of a kettle whistling in the background.",
        "[UN:Sarah]/me pours a cup of tea, continuing \"But then, moments like these, they're soothing. There's beauty in the calm.\"",
        "[UN:_]/env The faint sound of thunder in the distance.",
        "[UN:Sarah]/me with a hint of optimism \"Each storm eventually clears, right? Tomorrow is a new day.\"",
        "[UN:_]/env The cassette player hums quietly as the tape continues, blending with the sounds of the rain."
    }
})

table.insert(stories, {
    title = "Late Night Study Session",
    messages = {
        "[UN:_]/env The quiet buzz of a lamp and the rustle of pages turning.",
        "[UN:Alex]/me mutters under their breath \"Okay, just one more chapter, and I'm done for the night.\"",
        "[UN:_]/env The soft tapping of a pencil against a notebook.",
        "[UN:Alex]/me exhales deeply, frustration evident \"Why can't I remember this for the exam?\"",
        "[UN:_]/env The sudden, uplifting sound of music as Alex decides to take a break.",
        "[UN:Alex]/me 's voice, now more relaxed and hopeful \"A little music does wonders. Alright, back to it with fresh eyes.\"",
        "[UN:_]/env The cassette player clicks softly, a constant companion through the night's challenges."
    }
})

table.insert(stories, {
    title = "A Walk Down Memory Lane",
    messages = {
        "[UN:_]/env The crunch of gravel underfoot on a quiet path.",
        "[UN:Maggie]/me 's voice, filled with nostalgia \"This old road hasn't changed a bit. Just like our childhood summers...\"",
        "[UN:_]/env The distant bark of a dog, evoking memories of days gone by.",
        "[UN:Maggie]/me laughs softly \"Remember when we tried to build that treehouse here? What a summer that was.\"",
        "[UN:_]/env The soft rustling of leaves in the wind.",
        "[UN:Maggie]/me sighs contentedly \"It's funny how places hold so many memories. I'm glad we made so many here.\"",
        "[UN:_]/env The cassette player's quiet whirring as the tape winds down, capturing a moment frozen in time."
    }
})

table.insert(stories, {
    title = "Weekend Gardening",
    messages = {
        "[UN:_]/env Birds chirping, the sound of a shovel digging into the earth.",
        "[UN:Martha]/me instructs warmly \"Make sure to space the seeds properly, like this.\"",
        "[UN:Alex]/me , with curiosity \"And how long till we see the flowers?\"",
        "[UN:Martha]/me smiles, even though it can't be seen \"In a few weeks, we'll have a garden full of colors.\"",
        "[UN:_]/env The sound of watering plants, a gentle flow of water from a can.",
        "[UN:Alex]/me with a sense of achievement \"This is going to be the best garden ever!\"",
        "[UN:Martha]/me agrees, full of hope \"Yes, it will be. A little patience and love, that's all it takes.\""
    }
})

table.insert(stories, {
    title = "Pact of the Forgotten",
    messages = {
        "[UN:Marcus]/me , with a cautious tone, says \"We can't survive out there alone. It's about time we set some ground rules.\"",
        "[UN:Elena]/me nods, eyeing the others \"Agreed. But how do we ensure everyone sticks to the plan? Trust is scarce these days.\"",
        "[UN:Raj]/me interjects, optimistically \"First, we establish roles. I know a thing or two about first aid.\"",
        "[UN:Tara]/me , slightly hesitant, adds \"And I can handle food supplies. Been working in a grocery store my whole life.\"",
        "[UN:_]/env The crackle of a distant fire can be heard, with the occasional distant moan of zombies.",
        "[UN:Marcus]/me firmly states \"Alright, survival first. We need to secure shelter, water, and food. No one goes out alone.\"",
        "[UN:Elena]/me raises an eyebrow \"And what about when we encounter... others?\"",
        "[UN:Raj]/me suggests \"We help if we can, but our safety comes first. No unnecessary risks.\"",
        "[UN:Tara]/me quietly says \"We should also remember those we've lost. Keep a record, a... reminder of why we fight.\"",
        "[UN:_]/env A somber silence falls over the group, momentarily.",
        "[UN:Marcus]/me clears his throat \"Defense. We need to know how to protect ourselves. Any experience with weapons?\"",
        "[UN:Elena]/me with a hint of pride \"I've been practicing with a bow. It's silent and effective.\"",
        "[UN:Raj]/me admits \"I'm not much of a fighter, but I can make sure our defenses are solid. Barricades, traps...\"",
        "[UN:Tara]/me looks down, whispering \"I'm scared of guns. But I can learn. For the group.\"",
        "[UN:_]/env The sound of a can being kicked accidentally echoes, a tense moment follows.",
        "[UN:Marcus]/me nods understandingly \"Fear keeps us alive. We'll start training tomorrow. Slowly, together.\"",
        "[UN:Elena]/me suggests \"Communication is key. We should always have a way to reach each other, no matter what.\"",
        "[UN:Raj]/me , with hope in his voice \"And let's not forget about humanity. Music, stories... We need more than just survival.\"",
        "[UN:Tara]/me smiles faintly \"I found a guitar in one of the houses. Not sure if it's in tune, but...\"",
        "[UN:_]/env A long dawn silence echos.",
        "[UN:Marcus]/me decides \"We'll meet here, every night at sunset. Discuss the day, plan the next. It's our council.\"",
        "[UN:Elena]/me , with a newfound respect, adds \"We're more than survivors; we're a family now. We look out for each other.\"",
        "[UN:Raj]/me , feeling a sense of belonging, says \"It won't be easy. But together, we stand a chance.\"",
        "[UN:Tara]/me , with determination, states \"We'll make it. This is our new beginning. We'll rebuild, somehow.\"",
        "[UN:_]/env A distant howl pierces the night, a grim reminder of the world outside.",
        "[UN:Marcus]/me solemnly concludes \"Then it's settled. Tomorrow, we start anew. For us, for those we've lost, for hope.\"",
        "[UN:Elena]/me quietly adds \"We'll remember this night. The night we chose to fight back, to live, not just survive.\"",
        "[UN:Raj]/me places a hand over a makeshift map, tracing routes \"We've got a lot to do. But we're in this together.\"",
        "[UN:Tara]/me , picking up the guitar, strums a chord, off-tune but promising \"For the first time in months, I feel hopeful.\"",
        "[UN:_]/env As the fire crackles, the group sits in contemplation, united by their pact, surrounded by darkness yet guided by a shared light of hope."
    }
})

table.insert(stories, {
    title = "A Day to Remember",
    messages = {
        "[UN:Mark]/me looks out the window, noticing the clear blue sky, and says \"Perfect day for the picnic, isn't it?\"",
        "[UN:Jenny]/me , with excitement in her voice, responds \"Absolutely! I've packed everything. Are the kids ready?\"",
        "[UN:Mark]/me nods, smiling \"They're more than ready. Been up since dawn!\"",
        "[UN:_]/env The sound of children laughing and running down the stairs is heard.",
        "[UN:Timmy]/me shouts with joy \"Daddy, daddy, let's go!\"",
        "[UN:Suzy]/me , with a slight whine, says \"But I can't find my sun hat!\"",
        "[UN:Jenny]/me reassures calmly \"Don't worry, sweetie. I saw it in the living room.\"",
        "[UN:_]/env The family car starts, followed by a light-hearted chat and laughter.",
        "[UN:Mark]/me , while driving, asks \"Everyone excited for the lake?\"",
        "[UN:Family]/me responds in unison \"Yes!\"",
        "[UN:_]/env The sound of a car driving off, with a background of birds chirping and a gentle breeze.",
    }
})

table.insert(stories, {
    title = "The Last Light",
    messages = {
        "[UN:Rick]/me , with a tone of urgency, whispers \"Quiet. They're close.\"",
        "[UN:Sarah]/me , barely audible, asks \"Do you think this place is safe?\"",
        "[UN:Rick]/me replies with cautious optimism \"For now. But we need to keep moving at dawn.\"",
        "[UN:_]/env The faint sound of something shuffling outside.",
        "[UN:Old Man Jenkins]/me , coughing softly, says \"I don't know if I can make it another day.\"",
        "[UN:Sarah]/me offers comfortingly \"We'll find medicine soon. Hang in there.\"",
        "[UN:_]/env A soft thud against the door causes a moment of silence.",
        "[UN:Rick]/me checks his weapon, saying determinedly \"I'll take first watch.\"",
        "[UN:Sarah]/me , with a hint of fear, whispers \"What if they get in?\"",
        "[UN:Rick]/me reassures \"Then we fight. We survive. It's what we do.\"",
        "[UN:_]/env The recording continues with faint groans until the tape runs out.",
    }
})

table.insert(stories, {
    title = "Echoes of Survival",
    messages = {
        "[UN:Alex]/me , with a heavy heart, starts recording \"We're here in what remains of... doesn't matter anymore. I'm with Morgan, who... who's seen better days.\"",
        "[UN:Morgan]/me coughs weakly, forcing a smile \"Seen better, faced worse. Or so I'd like to believe.\"",
        "[UN:Alex]/me asks gently \"Can you tell us your story? How did you end up here?\"",
        "[UN:Morgan]/me takes a slow breath \"It started like any other day... until it wasn't. The world turned inside out in hours.\"",
        "[UN:_]/env The sound of a distant fire crackling, with occasional groans beyond the safety of their shelter.",
        "[UN:Morgan]/me continues \"I was with a group then. We thought we could make it to a safe zone. We were wrong.\"",
        "[UN:Alex]/me probes further \"What happened to your group?\"",
        "[UN:Morgan]/me 's voice breaks slightly \"Ambushed. Not by the dead, but by the living. For a can of food... a can of food.\"",
        "[UN:Alex]/me , with sorrow in his voice, asks \"How did you survive?\"",
        "[UN:Morgan]/me looks away, tears mingling with dirt on her face \"I ran. I left them... my family. It haunts me every night.\"",
        "[UN:_]/env A somber silence fills the room, broken only by the crackling fire.",
        "[UN:Alex]/me changes the subject \"You've seen a lot. What kept you going all this time?\"",
        "[UN:Morgan]/me ponders, then answers \"Hope. Hope that I'd find a reason to forgive myself. Hope that I'd make a difference.\"",
        "[UN:Alex]/me asks \"Do you think you've found that reason?\"",
        "[UN:Morgan]/me smiles weakly \"In bits and pieces. Helping others, sharing my story... it's a start.\"",
        "[UN:_]/env The wind howls outside, a reminder of the world's harsh reality.",
        "[UN:Alex]/me inquires softly \"If you had any advice for those still out there, what would it be?\"",
        "[UN:Morgan]/me reflects deeply \"Don't lose yourself. It's easy to become something you despise. Remember who you are, who you want to be.\"",
        "[UN:Alex]/me nods, adding \"And for those who've lost hope?\"",
        "[UN:Morgan]/me 's voice strengthens \"Find something to fight for. Even in the darkest times, there's light to be found. Don't give up.\"",
        "[UN:_]/env The shelter creaks, a testament to the struggle of standing amidst decay.",
        "[UN:Alex]/me , with respect in his tone, asks \"Is there anyone you'd like to mention? Someone you're thinking of now?\"",
        "[UN:Morgan]/me closes her eyes, a single tear rolling down \"Ella... my little girl. I hope she's out there, safe, living a life I couldn't give her.\"",
        "[UN:Alex]/me , visibly moved, whispers \"Thank you, Morgan. For sharing your story.\"",
        "[UN:Morgan]/me , with fading strength, murmurs \"Thank you for listening. Maybe someone will hear this... maybe it'll make a difference.\"",
        "[UN:_]/env A moment of heavy silence, as if the world itself is listening.",
        "[UN:Alex]/me concludes \"This is Alex, signing off. Wherever you are, keep fighting. You're not alone.\"",
        "[UN:_]/env A thud is heard, as if the recorder was set down, but the fire continues to crackle, a beacon in the night.",
        "[UN:Morgan]/me , now barely audible, whispers \"Not alone...\"",
        "[UN:_]/env The sound of the fire fades into the quiet night, leaving a lingering echo of hope amidst despair as the tape ends.",
    }
})

table.insert(stories, {
    title = "Summer BBQ",
    messages = {
        "[UN:Mike]/me , excitedly setting up the grill, declares \"This is going to be the best BBQ yet. Wait and see.\"",
        "[UN:Carol]/me laughs, arranging the patio furniture, \"I believe you, but don't burn the burgers this time!\"",
        "[UN:_]/env Sounds of children playing and a dog barking happily in the yard.",
        "[UN:Dave]/me boasts \"Brought the secret sauce. It's game over for those steaks.\"",
        "[UN:Jen]/me teases \"Hope your secret sauce is better than your last 'culinary experiment.'\"",
        "[UN:_]/env The sizzle of meat hitting the hot grill fills the air, mingling with the scent of smoky charcoal.",
        "[UN:Mike]/me , with mock offense, retorts \"My experiments are what make this BBQ legendary.\"",
        "[UN:Carol]/me , chuckling, adds \"True, but your experiments also include calling the fire department.\"",
        "[UN:_]/env Laughter erupts around the grill, as a playlist of classic hits begins to play softly in the background.",
        "[UN:Dave]/me , raising a glass, proposes \"To good friends, good food, and no fire department visits today!\"",
        "[UN:All]/me respond in unison, clinking glasses, \"Cheers!\"",
    }
})

table.insert(stories, {
    title = "Road Trip Adventure",
    messages = {
        "[UN:Leo]/me , starting the engine, exclaims \"Adventure awaits! Let's hit the road.\"",
        "[UN:Mia]/me says \"First stop, the Grand Canyon!\"",
        "[UN:_]/env The sound of a car driving off, with upbeat road trip music playing.",
        "[UN:Zack]/me asks \"Did we remember the snacks? You can't road trip without snacks!\"",
        "[UN:Leo]/me , laughing, reassures \"Packed and ready. We have enough snacks to feed a small army.\"",
        "[UN:_]/env Occasional chatter, laughter, and the GPS giving directions fill the car.",
        "[UN:Mia]/me exclaims \"Look, a roadside diner! Let's grab some real food.\"",
        "[UN:Zack]/me , excitedly unbuckling, agrees \"Best idea you've had all day!\"",
        "[UN:_]/env The sound of a diner bell as they enter, followed by the murmur of patrons and clinking dishes.",
        "[UN:Leo]/me declares \"That was the best burger I've ever had. Hands down.\"",
        "[UN:Mia]/me , content, adds \"This trip is already amazing. Can't wait for what's next.\"",
    }
})

table.insert(stories, {
    title = "The Road Home",
    messages = {
        "[UN:Johnny]/me starts off strong singing \"I hit the road when the dawn breaks, with my heart wide open and free.\"",
        "[UN:_]/env The twang of the guitar echoes, blending with the distant crow of a rooster.",
        "[UN:Johnny]/me continues \"Past the fields where my father worked, under the sky so vast and deep.\"",
        "[UN:_]/env Soft sounds of a harmonica join in, painting pictures of wide-open spaces.",
        "[UN:Johnny]/me 's voice grows tender, \"I've seen mountains high, rivers wide, but nothing feels like the road home to me.\"",
        "[UN:_]/env The tempo picks up, feet tapping, as the room fills with the warmth of the melody.",
        "[UN:Johnny]/me sings \"With every mile, every town, your love's the beacon calling me home.\"",
        "[UN:_]/env A fiddle weaves in, lifting the tune into a joyful dance.",
        "[UN:Johnny]/me 's voice fills with emotion, he belts out \"Through the laughter and the tears, in your arms is where I belong.\"",
        "[UN:_]/env The music builds, a crescendo of strings and harmonies, embracing the listener.",
        "[UN:Johnny]/me concludes softly \"So I'm coming home, back to where my heart has always been. In that summer BBQ, under the stars, with you.\"",
        "[UN:_]/env The final note lingers, blending with the sounds of a night coming alive, as the story fades to a close.",
    }
})

table.insert(stories, {
    title = "Zombie Night",
    messages = {
        "[UN:Rex]/me kicks off with a growl, \"When the moon is full and the night comes alive, with the undead roaming the streets.\"",
        "[UN:_]/env The crash of drums thunder, electric guitars scream.",
        "[UN:Rex]/me howls, \"We lace up boots, guitars in hand, ready to fight, ready to stand.\"",
        "[UN:_]/env The bass rumbles, deep and ominous, like a gathering storm.",
        "[UN:Rex]/me 's voice turns fierce, \"Through the darkness, through the fear, we play our anthem loud and clear.\"",
        "[UN:Rex]/me shouts, \"With every chord, every beat, we push back the horde, we claim the street.\"",
        "[UN:_]/env Guitar solos wail, a duel with the night, notes soaring, slicing through the gloom.",
        "[UN:Rex]/me 's voice, gritty with passion, belts, \"In the heart of the fight, we find our song, together we're mighty, together we're strong.\"",
        "[UN:_]/env The rhythm builds, a frenzied pace, drums and heartbeats syncing as one.",
        "[UN:Rex]/me declares triumphantly, \"We stand as one, till the break of dawn, till the zombies are gone, our spirits never worn.\"",
        "[UN:_]/env As the final chord strikes, the sound of disance moans get louder, and the tape ends.",
    }
})

-- this complicated shit is needed because PZ called OnCreate everytime a tape
-- is initialized, instead of just the first time the item is created, but worst
-- of all, it calls it before the moddata is applied, so we can not know if at
-- time of OnCreate if this is actually a new tape or not.
local toInitializeItems = {}
local isChecking = false

local function WRC_CheckItems()
    for i = #toInitializeItems, 1, -1 do
        if toInitializeItems[i][2] <= 0 then
            local item = toInitializeItems[i][1]
            local md = item:getModData()
            if not md["WRC_Processed"] and ZombRand(50) == 0 then
                WRC_RandomizeTape(item)
            end
            md["WRC_Processed"] = true
            table.remove(toInitializeItems, i)
        else
            toInitializeItems[i][2] = toInitializeItems[i][2] - 1
        end
    end
    if #toInitializeItems == 0 then
        isChecking = false
        Events.OnTick.Remove(WRC_CheckItems)
    end
end

function WRC_OnCreateTape(item)
    table.insert(toInitializeItems, {item, 60})
    if not isChecking then
        isChecking = true
        Events.OnTick.Add(WRC_CheckItems)
    end
end

function WRC_RandomizeTape(item)
    local md = item:getModData()
    local randomStory = stories[ZombRand(#stories) + 1]
    item:setCustomName(true)
    item:setName(randomStory.title)
    for i = 1, #randomStory.messages do
        md["WRC_Message" .. i] = randomStory.messages[i]
    end
    md["WRC_TapeUsed"] = #randomStory.messages
end