# Wasteland's RP Chat

Written for and maintained by Wasteland RP, this mod overhauls and adds many new features to the in-game chat system to improve the RP experience.

- Multiple levels of speaking volumes: whisper, quiet, normal, yell, shout
- Variety of types of chat: Speaking, Emoting, Environmental
- Addon Chats: Radio, OOC, Staff, Private
- In game indication of typing and automatic action emotes
- In game indication of speaking/emoting out of Range
- Customize name, colors, and many other aspects of the RP experience
- Tape Recorders
- Dice rolling
- Multiple RP languages
- Quick action menu (Trade, grow hair, injure self, etc)
- Special admin hammer for staff
- And much more!

How to use? Everything available in Wasteland's RP Chat is available in the chat window. Click the Gear in the chat window to see many of the settings then type /help and /howto into the chat window to see available commands.

How to install? For players, just join a server which is using this mod. For server owners, subscribe to the mod and add it to your server's workshop collection. Due to an unfixable implementation detail, there is an optional server-side only java-mod which can be installed into your server which disabled the radio's static due to distance. Move the ZomboidRadio.class (located in the workshop mod files) and replace the file with the same name at "Project Zomboid Dedicated Server\java\zombie\radio" in your dedicated servers directory.

## License

Copyright 2023 Kevin Gravier & Wasteland RP
Unauthorized copying, reproduction, or distribution of this software is strictly prohibited without written permission.
